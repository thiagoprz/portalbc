<?php


namespace Thiagoprz\PortalBC\Http;


use GuzzleHttp\Client as HttpClient;

/**
 * Class Client
 * @package Thiagoprz\PortalBC\Http
 */
class Client
{

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var string
     */
    private $authToken;

    private $baseUrl;


    protected $log;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->baseUrl = config('portalbc.api_url');
        $this->httpClient = new HttpClient([
            'base_url' => config('portalbc.api_url'),
        ]);
        $this->authToken = $this->authenticate();
        $this->logMessage(json_encode($this->authToken));
    }

    private function logMessage($message)
    {
        $this->log .= $message . PHP_EOL;
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function authenticate()
    {
        $this->logMessage("Realizando autenticação na url: $this->baseUrl/v1/login");
        $auth = $this->httpClient->post("$this->baseUrl/v1/login", [
            'body' => json_encode([
                'login' => config('portalbc.login'),
                'password' => config('portalbc.password'),
            ]),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
        $res = (string)$auth->getBody();
        $response = json_decode($res);
        $this->logMessage("Resposta: $res");
        if ($response && $response->status) {
            return $response->token;
        }
    }

    /**
     * @param boolean $forCurl
     * @return string[]
     */
    private function getHeaders($forCurl = false)
    {
        return $forCurl ? [
            "Authorization: Bearer $this->authToken",
            'Accept: application/json',
        ] : [
            'Authorization' => "Bearer $this->authToken",
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * @param $url
     * @param $body
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($url, $body)
    {
        $this->logMessage("GET: $this->baseUrl$url");
        $params = http_build_query($body);
        $request = $this->httpClient->get("$this->baseUrl$url?" . $params, [
            'headers' => $this->getHeaders(),
        ]);
        $res = (string)$request->getBody();
        $this->logMessage("Resposta: $res");
        return json_decode($res);
    }

    /**
     * @param $url
     * @param $body
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($url, $body)
    {
        $request = $this->httpClient->request('POST', "$this->baseUrl$url", [
            'body' => json_encode($body),
            'headers' => $this->getHeaders(),
        ]);
        $res = (string)$request->getBody();
        $this->logMessage("Resposta: $res");
        return json_decode($res);
    }

    /**
     * @param $url
     * @param $body
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put($url, $body)
    {
        $request = $this->httpClient->request('PUT', "$this->baseUrl$url", [
            'body' => json_encode($body),
            'headers' => $this->getHeaders(),
        ]);
        $res = (string)$request->getBody();
        $this->logMessage("Resposta: $res");
        return json_decode($res);
    }

    /**
     * @param $url
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($url)
    {
        $request = $this->httpClient->request('DELETE', "$this->baseUrl$url", [
            'headers' => $this->getHeaders(),
        ]);
        $res = (string)$request->getBody();
        $this->logMessage("Resposta: $res");
        return json_decode($res);
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Upload de arquivos
     * @param string $url
     * @param string $file
     * @param string $name
     * @return mixed
     */
    public function upload(string $url, string $file, string $name)
    {
        if (function_exists('curl_file_create')) { // php 5.5+
            $cFile = curl_file_create($file);
        } else { //
            $cFile = '@' . realpath($file);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$this->baseUrl$url");
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            $name => $cFile,
        ]);
        $headers = $this->getHeaders(true);
        unset($headers['Content-Type']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);
        curl_close($ch);
        $this->logMessage("Resposta: $res");
        return json_decode($res);
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->authToken;
    }

    /**
     * @param string $authToken
     */
    public function setAuthToken(string $authToken): void
    {
        $this->authToken = $authToken;
    }
}
