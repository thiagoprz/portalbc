<?php


namespace Thiagoprz\PortalBC;


use Illuminate\Support\ServiceProvider;

class PortalBCServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/portalbc.php' => config_path('portalbc.php'),
        ]);
    }

}
