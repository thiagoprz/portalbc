<?php
return [
    'api_url' => env('PORTALBC_API_URL', 'https://portalbc.online/api/clubebc'),
    'login' => env('PORTALBC_API_LOGIN', 'portalbc'),
    'password' => env('PORTALBC_API_PASSWORD', 'portalbc'),
];
